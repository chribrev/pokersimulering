import no.ntnu.idatt2001.pokerSimulation.PlayingCard;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class PlayingCardTest {

    @Test
    @DisplayName("Testing toString Method")
    public void TestingPlayingCardToString() {
        PlayingCard playingcard = new PlayingCard("H", 4);
        assertEquals(playingcard.toString(), "H4");
    }

    @Test
    @DisplayName("Testing Exception when using wrong type")
    public void TestingPlayingCardType() {
        try {
            PlayingCard playingcard = new PlayingCard("F", 4);
            fail("Type wasn't caught");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "The type of card has to be H,D,C or S");
        }
    }

    @Test
    @DisplayName("Testing Exception when using wrong number")
    public void TestingPlayingCardNumber() {
        try {
            PlayingCard playingcard = new PlayingCard("H", 25);
            fail("Type wasn't caught");
        } catch (Exception e) {
            assertEquals(e.getMessage(), "The face has to be between 1 and 13");
        }
    }
}