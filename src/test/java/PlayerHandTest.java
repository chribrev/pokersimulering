import no.ntnu.idatt2001.pokerSimulation.PlayerHand;
import no.ntnu.idatt2001.pokerSimulation.PlayingCard;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class PlayerHandTest {

    @Test
    @DisplayName("Testing That Flush methods returns Yes with flush")
    public void checkingFlushMethod(){
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 1; i<6; i++){
            cards.add(new PlayingCard("H", i));
        }
        PlayerHand hand = new PlayerHand(cards);
        assertEquals("Yes", hand.checkFlush());
    }

    @Test
    @DisplayName("Testing That Flush method returns No without flush")
    public void checkingFalseFlushMethod(){
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 1; i<5; i++){
            cards.add(new PlayingCard("H", i));
        }
        cards.add(new PlayingCard("D", 1));
        PlayerHand hand = new PlayerHand(cards);
        assertEquals("No", hand.checkFlush());
    }

    @Test
    @DisplayName("Testing That Flush method returns No without flush")
    public void checkingthatHandValueIsRight(){
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 1; i<6; i++){
            cards.add(new PlayingCard("H", i));
        }
        PlayerHand hand = new PlayerHand(cards);
        assertEquals(15, hand.handValue());
    }

    @Test
    @DisplayName("Testing That queenOfSpades returns Yes if the queen of spades is present")
    public void checkingQueenOfSpadesMethod(){
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 13; i > 9; i--){
            cards.add(new PlayingCard("S", i));
        }
        PlayerHand hand = new PlayerHand(cards);
        assertEquals("Yes", hand.queenOfSpades());
    }

    @Test
    @DisplayName("Testing That queenOfSpades returns No if the queen of spades is not present")
    public void checkingFalseQueenOfSpadesMethod(){
        ArrayList<PlayingCard> cards = new ArrayList<>();
        for (int i = 13; i > 9; i--){
            cards.add(new PlayingCard("H", i));
        }
        PlayerHand hand = new PlayerHand(cards);
        assertEquals("No", hand.queenOfSpades());
    }
}
