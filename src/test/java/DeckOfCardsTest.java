import no.ntnu.idatt2001.pokerSimulation.DeckOfCards;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DeckOfCardsTest {
    @Test
    public void TestingDeckConstructor(){
        DeckOfCards deck = new DeckOfCards();
        assertEquals(deck.getDeck().size(), 52);
    }


    @Test
    public void TestingDrawingCard(){
        DeckOfCards deck = new DeckOfCards();
        assertSame(deck.dealHand(1).getClass(), ArrayList.class);
    }

    @Test
    public void TestingThatAllDrawnCardsAreDifferent(){
        DeckOfCards deck = new DeckOfCards();
        boolean someAreEqual = false;
        int i = 50;
        while(!deck.getDeck().isEmpty() | i <= 0){
            if (deck.dealHand(1).equals(deck.dealHand(1))){
                someAreEqual = true;
            }
            i++;
        }
        assertFalse(someAreEqual);
    }

    @Test
    public void TestingThatDrawingCardShortensDeck(){
        DeckOfCards deck = new DeckOfCards();
        for(int i = 0; i < 10; i++){
            deck.dealHand(1);
        }
        assertEquals(deck.getDeck().size(), 42);
    }

    @Test
    public void TestingThatAllCardsWillBeDrawnEventually(){
        DeckOfCards deck = new DeckOfCards();
        for(int i = 0; i < 52; i++){
            deck.dealHand(1);
        }
        assertNull(deck.dealHand(1));
    }
}