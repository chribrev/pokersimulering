package no.ntnu.idatt2001.pokerSimulation;

import java.util.ArrayList;

public class PlayerHand {
    ArrayList<PlayingCard> hand;

    public PlayerHand() {
        this.hand = new ArrayList<>();
    }

    public PlayerHand(ArrayList<PlayingCard> cards) {
        this.hand = cards;
    }

    /**
     * Method for acquiring the hand
     * @return a list of playing cards
     */
    public ArrayList<PlayingCard> showHand(){
        return hand;
    }

    /**
     * A method for checking if the hand has a flush
     * @return true if there is a flush, false if not
     */
    public String checkFlush(){
        int hearts = 0;
        int clovers = 0;
        int spades = 0;
        int diamonds = 0;

        for(PlayingCard card : hand){
            switch (card.getSuit()) {
                case "H":
                    hearts += 1;
                    break;
                case "D":
                    diamonds += 1;
                    break;
                case "C":
                    clovers += 1;
                    break;
                case "S":
                    spades += 1;
                    break;
            }
        }

        if (hearts > 4 | clovers > 4 | spades > 4 | diamonds > 4){
            return "Yes";
        }
        return "No";
    }

    /**
     * Method for finding the total value of all cards on the hand
     * @return the value of all card values combined
     */
    public int handValue(){
        int totalvalue = 0;
        for(PlayingCard card : hand){
            totalvalue += card.getFace();
        }
        return totalvalue;
    }

    /**
     * Method for checking if the queen of spades is in the hand
     * @return true if the queen of spades in the hand, false if not
     */
    public String queenOfSpades(){
        for(PlayingCard card : hand){
            if(card.getFace() == 12 && card.getSuit().equals("S")){
                return "Yes";
            }
        }
        return "No";
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder();
        for (PlayingCard card : hand){
            returnString.append(String.format("%s ", card));
        }
        return returnString.toString();
    }

    /**
     * Method for acquiring all cards in the hand with the suit Heart
     * @return a list of PlayingCards
     */
    public String cardsOfHeart(){
        StringBuilder cardsOfHeart = new StringBuilder();
        for(PlayingCard card : hand){
            if(card.getSuit().equals("H")){
                cardsOfHeart.append(card).append(" ");
            }
        }
        if (cardsOfHeart.length() < 1){
            return "None";
        }
        return cardsOfHeart.toString();
    }
}
