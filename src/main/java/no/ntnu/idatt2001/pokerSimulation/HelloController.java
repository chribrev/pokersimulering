package no.ntnu.idatt2001.pokerSimulation;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class HelloController {
    private PlayerHand hand;

    @FXML
    private TextField playerHand;
    @FXML
    private TextField queenOfSpadesLabel;
    @FXML
    private TextField cardsOfHeartsLabel;
    @FXML
    private TextField flushLabel;
    @FXML
    private TextField sumOfFacesLabel;

    @FXML
    protected void dealHand(ActionEvent actionEvent) {
        DeckOfCards deck = new DeckOfCards();
        hand = new PlayerHand(deck.dealHand(5));
        playerHand.setText(hand.toString());
    }

    @FXML
    public void checkHand(ActionEvent actionEvent) {
        if(!hand.hand.isEmpty()){
            sumOfFacesLabel.setText(String.format("%s",hand.handValue()));
            cardsOfHeartsLabel.setText(hand.cardsOfHeart());
            queenOfSpadesLabel.setText(hand.queenOfSpades());
            flushLabel.setText(hand.checkFlush());
        }
    }
}