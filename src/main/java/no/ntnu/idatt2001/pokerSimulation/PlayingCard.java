package no.ntnu.idatt2001.pokerSimulation;

import java.util.Locale;

public class PlayingCard {
    private final String suit; // 'S'=spade, 'H'=heart, 'D'=diamonds, 'C'=clubs
    private final int face; // a number between 1 and 13

    /**
     * Constructor for a class which represents a playing card
     * @param suit a letter which must be either H,D,C or S
     * @param face a number between 1 and 13
     */
    public PlayingCard(String suit, int face) {
        if (suit.equals("H") | suit.equals("D") | suit.equals("C") | suit.equals("S")){
            this.suit = suit.toUpperCase(Locale.ROOT);
        }
        else {
            throw new IllegalArgumentException("The type of card has to be H,D,C or S");
        }
        if(face < 0 | face > 14){
            throw new IllegalArgumentException("The face has to be between 1 and 13");
        }
        else {
            this.face = face;
        }
    }

    @Override
    public String toString() {
        return suit + face;
    }

    public String getSuit() {
        return suit;
    }

    public int getFace() {
        return face;
    }
}