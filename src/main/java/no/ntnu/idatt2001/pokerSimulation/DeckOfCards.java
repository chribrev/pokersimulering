package no.ntnu.idatt2001.pokerSimulation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class DeckOfCards {
    ArrayList<PlayingCard> fullDeck = new ArrayList<>();
    ArrayList<PlayingCard> deck = new ArrayList<>();

    /**
     * A class which represents a deck of cards
     * The class has two decks, one which will be drawn from,
     * another which will be used to fill the main deck
     */

    public DeckOfCards() {
        for(int i=1; i<14;i++){
            //Adding Cards to deck
            deck.add(new PlayingCard("H",i));
            deck.add(new PlayingCard("D",i));
            deck.add(new PlayingCard("C",i));
            deck.add(new PlayingCard("S",i));

            //Adding Cards to fullDeck
            fullDeck.add(new PlayingCard("H",i));
            fullDeck.add(new PlayingCard("D",i));
            fullDeck.add(new PlayingCard("C",i));
            fullDeck.add(new PlayingCard("S",i));
        }
    }

    public ArrayList<PlayingCard> dealHand(int n) throws IllegalArgumentException{
        if(n < 1) throw new IllegalArgumentException("Cannot pick less than one card, please let hand-size be any "
                + "integer from 1 to 52");
        if(n > 52) throw new IllegalArgumentException("Cannot pick more than 52 cards for a hand, since that " +
                "is the size of any given deck, please let hand-size be an integer from 1 to 52");
        ArrayList<PlayingCard> newCards = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            if(deck.size()>0){
                int cardPos = random.nextInt(deck.size());
                newCards.add(deck.get(cardPos));
                deck.remove(cardPos);
            }
        }
        if (newCards.isEmpty()){
            return null;
        }
        return newCards;
    }

    /**
     * Fills the deck with new cards
     */
    public void reshuffle(){
        deck.clear();
        Collections.copy(deck,fullDeck);
    }

    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    public void setDeck(ArrayList<PlayingCard> deck) {
        this.deck = deck;
    }
}